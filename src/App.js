import Navbar from './components/Navbar';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import AddTodo from './pages/AddTodo';

function App() {
  return (
    <Router>
      <Navbar/>
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/addtodo' element={<AddTodo/>}/>
        <Route path='*' element={'404: Not Found'}/>
      </Routes>
    </Router>
  );
}

export default App;
