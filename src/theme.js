import { createTheme } from '@mui/material';
import { grey, blue } from '@mui/material/colors';

export const theme = createTheme({
    palette: {
        primary: {
          main: blue[500],
        },
        secondary: {
          main: grey[100],
        },
    },
})