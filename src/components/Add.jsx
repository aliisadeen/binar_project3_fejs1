import { Button, TextField, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'
import { useNavigate } from 'react-router-dom'

const Add = () => {
    const navigate = useNavigate()

    return (
        <Box flex={4} p={4}>
            <Typography variant='h4' textAlign='center' paddingBottom={4}>
                    Todo Input
            </Typography>

            <TextField id='outlined-basic' label='Input Todo' variant='outlined' sx={{width:'100%'}}></TextField>

            <Button variant='contained' color='primary' onClick={() => navigate('/')} sx={{display:'flex', marginTop:'1rem', width:'100%', height:'3rem'}} >Submit</Button>
        </Box>
    )
}

export default Add
