import { Delete, Edit } from '@mui/icons-material';
import { Box, Button, ButtonGroup, IconButton, List, ListItem, ListItemText, Typography, Checkbox  } from '@mui/material';
import React, { useState, useEffect } from 'react';
import data from '../data/data.json';


const commonStyles = {
    bgcolor: 'background.secondary',
    m: 1,
    borderColor: 'text.primary',
    width: '100%'
};

const Feed = () => {
    const [tempData, setTempData] = useState([]);
    useEffect(() => {
        setTempData(data);
    }, []);

    const handleFilterAll = () => {
        setTempData(data);
    };

    const handleFilterDone = () => {
        const done = tempData.filter((item) => item.complete === true);
        setTempData(done);
    };

    const handleFilterTodo = () => {
        const todo = tempData.filter((item) => item.complete === false);
        setTempData(todo);
    };
    
    const handleDelete = (id) => {
        const newData = tempData.filter((item) => item.id !== id);
        setTempData(newData);
    };

    return (
        <Box flex={4} p={4}>
            <Typography variant='h4' textAlign='center' paddingBottom={4}>
                Todo List
            </Typography>

            <ButtonGroup sx={{display:'flex', justifyContent:'space-between', marginBottom:'1rem'}}>
                <Button variant='contained' color='primary' sx={{ width: '32%', height: '50px' }}
                onClick={() => handleFilterAll()}
                >All</Button>
                <Button variant='contained' color='primary' sx={{ width: '32%', height: '50px' }}
                onClick={() => handleFilterDone()}
                >Done</Button>
                <Button variant='contained' color='primary' sx={{ width: '32%', height: '50px' }}
                onClick={() => handleFilterTodo()}
                >Todo</Button>
            </ButtonGroup>

            <List sx={{ width: '100%', bgcolor: 'background.paper', display:'grid', gap:'0.5rem', marginBottom:'1rem' }}>
                {tempData.map((item) => (
                    <ListItem key={item.id} sx={{ ...commonStyles, border: 1, borderRadius: '5px', margin:'0', paddingLeft:2, paddingRight:2}}>
                        <ListItemText primary={`${item.task}`}/>
                        <Checkbox
                        checked={item.complete}
                        // onClick={handleChecked(item.id)}
                        />
                        <IconButton>
                            <Edit color='primary'/>
                        </IconButton>
                        <IconButton>
                            <Delete color='warning'
                            onClick={() => handleDelete(item.id)}
                            />
                        </IconButton>
                    </ListItem>
                ))}
            </List>

            <ButtonGroup color='error' sx={{display:'flex', justifyContent:'space-between'}}>
                <Button variant='contained' sx={{ width: '49%', height: '50px' }}
                onClick={() => handleFilterTodo()}
                >Delete done tasks</Button>
                <Button variant='contained' sx={{width:'49%', height:'50px'}}
                onClick={() => setTempData([])}
                >Delete all tasks</Button>
            </ButtonGroup>
        </Box>
    )
}

export default Feed;